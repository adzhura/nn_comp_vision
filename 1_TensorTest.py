import torch as torch

print("Hello, world!")
print(torch.__version__)

X = torch.tensor([[1., 2., 3.], [4., 5., 6.], [7., 8., 9.]], requires_grad=True)
print((X**2).prod())
limit = 5
print(X > limit)
print(X[X > limit])

larger_than_limit_sum = X[X > limit].sum()

print(larger_than_limit_sum)

print(torch.cuda.is_available())